package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Stack stack = new Stack();
        stack.push("tomasz");
        stack.push("hajto");
        stack.push("przejechal");
        stack.push("babe");
        stack.push("na");
        stack.push("pasach");

        System.out.println("rozmiar stosu:"+stack.size());
        System.out.println("wartosci na stosie:"+stack.get());
        System.out.println("usuniecie elementu na sczycie:"+stack.pop());
        System.out.println("wartosci na stosie:"+stack.get());
        System.out.println("rozmiar stosu:"+stack.size());
        stack.push("przejściuy na pieszych");
        System.out.println("wartosci na stosie:"+stack.get());
        System.out.println("rozmiar stosu:"+stack.size());
    }
}
