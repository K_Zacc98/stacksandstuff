package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Stack implements StackOperations{
    private List<String> elements = new ArrayList<>();

    @Override
    public List<String> get() {
        return elements;
    }

    public int size() {
        return elements.size();
    }

    @Override
    public Optional<String> pop() {
        if (elements.isEmpty()) {
            return Optional.empty();
        }
        String top = elements.get(elements.size() - 1);
        elements.remove(elements.size() - 1);
        return Optional.ofNullable(top);
    }

    @Override
    public void push(String item) {
        elements.add(item);
    }

}
